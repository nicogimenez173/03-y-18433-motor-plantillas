const db = require("../db/data");
const express = require ("express");
const router = express.Router();



router.get("/", (req, res) => {
    res.render("index",{integrantes: db.integrantes,});
});

const matriculas = [...new Set(db.media2.map(item => item.matricula))];
router.get("/:matricula", (request, response) => {
    const matricula = request.params.matricula;
    console.log(matricula)
    if (matriculas.includes(matricula)) {
        const media2Filtrada = db.media2.filter(item => item.matricula === matricula);
        const integrantesFiltrados = db.integrantes.filter(item => item.matricula === matricula);
        response.render('conjunto', {
            media: db.media,
            integrantes: integrantesFiltrados,
            media2: media2Filtrada,
        });
    }else{
      app.use((req, res, next) => {
        res.status(404).render('partials/error');
    });  
    }
    
});
console.log ("base de datos simulada", db);
console.log (db.integrantes);

module.exports = router;
















module.exports = router;
