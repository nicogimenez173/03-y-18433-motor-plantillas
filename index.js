const express = require("express");
const app = express();
const hbs = require ("hbs");
const router = require('./routes/public');
require ("dotenv").config();


app.use ("/", router);
app.set('view engine', 'hbs');
hbs.registerPartials(__dirname + "/views/partials");

app.use(express.static(__dirname + "/public"));
app.set('views', __dirname + "/views");

const puerto = process.env.PORT || 3000;

app.listen(puerto, () => {
    console.log(`Servidor coriendo en el puerto ${puerto}`);
    console.log(`http://localhost:${puerto}/`);
});
    
